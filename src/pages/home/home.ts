import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { ToastController, Platform } from 'ionic-angular';
import { HistorialService } from '../../providers/historial/historial';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private barcodeScanner: BarcodeScanner,
              public toastCtrl: ToastController,
              private platform: Platform,
              private _hs: HistorialService) {

  }

  scan(){
    if(!this.platform.is('cordova')){
      return;
    }

    this.barcodeScanner.scan().then(barcodeData => {
        if(!barcodeData.cancelled){
          this._hs.push(barcodeData.text);
          this.presentToast('Escaneo Compleado Con Exito');
        }

    }).catch(err => {
        this.presentToast(`Error : ${err}`);
    });
  }

  presentToast(msn:string){
    const toast = this.toastCtrl.create({
      message : msn,
      duration : 3000
    });
    toast.present();
  }

}
