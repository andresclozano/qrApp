import { Component } from '@angular/core';
import { HistorialService } from '../../providers/historial/historial';
import { ScanData } from '../../models/scan-data.model';

/**
 * Generated class for the HistorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-historial',
  templateUrl: 'historial.html',
})
export class HistorialPage {

  data:ScanData[] = [];

  constructor(private _hs: HistorialService) {
    this.data = this._hs.get();
  }

}
