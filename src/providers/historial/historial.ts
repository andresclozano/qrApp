import { Injectable } from '@angular/core';
import { ScanData } from '../../models/scan-data.model'

@Injectable()
export class HistorialService {

  private _historial:ScanData[] = [];

  constructor() {
  }

  get(){
    return this._historial;
  }

  push(texto:string){
    let data = new ScanData(texto);
    this._historial.unshift(data);
  }

}
