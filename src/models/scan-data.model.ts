export class ScanData {

  info:string;
  tipo:string;

  constructor(texto:string){
    this.tipo = this.getTipo(texto);
    this.info = texto;
  }

  private getTipo(text){
    if(texto.startsWith("http")){
      this.tipo = "http";
    }else if(text.startsWith("geo")){
      this.tipo = "map";
    }else{
      this.tipo = "no-definido";
    }
  }
}
